using System.Collections.Generic;
using Game;
using UnityEngine;

[ExecuteInEditMode]
public class DrawPath : MonoBehaviour
{
    [SerializeField] private List<Vector2> pointList = new List<Vector2>();
    [Range(0.1f,200f)]
    [SerializeField] private float segmentDistance;
    [SerializeField] private Color pathColor, segmentColor;

    private readonly Path path = new Path();

    private void OnDrawGizmos()
    {
        Gizmos.color = pathColor;

        for (var i = 0; i < path.Count-1; i++)
        {
            Gizmos.DrawLine(path[i],path[i+1]);
        }
        segmentDistance = Mathf.Max(segmentDistance, 0.3f);
        var points = path.FindSegmentPoints(segmentDistance
            ,maxPathLengthOfSegment:1.1f*segmentDistance,maxCount:20
            );
        if(points==null)
            return;
        Gizmos.color = segmentColor;
        for (var i = 0; i < points.Length; i++)
        {
            Gizmos.DrawWireSphere(points[i],0.5f);
        }
        for (var i = 0; i < points.Length-1; i++)
        {
            Gizmos.DrawLine(points[i],points[i+1]);
        }
    }

    private void OnValidate()
    {
        if (segmentDistance <= 0)
            segmentDistance = 0.1f;

            path.Clear();
        pointList?.ForEach((point)=>path.Add(point));
    }
}