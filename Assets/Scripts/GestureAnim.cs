﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class GestureAnim : MonoBehaviour
{
    private Animator _anim;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        var list = new[] {"Left", "Up", "Right", "Down"};


        while (true)
        {
            foreach (var s in list)
            {
                _anim.SetTrigger(s);
                yield return new WaitForSeconds(1f);
            }
          
        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
