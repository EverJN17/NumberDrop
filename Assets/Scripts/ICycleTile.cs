﻿using UnityEngine;

public interface ICycleTile
{
    float height { get; }
    Vector2 position { get; }
    void SetPosition(Vector2 position);
}