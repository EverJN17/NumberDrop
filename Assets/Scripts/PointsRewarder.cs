using UnityEngine;

public class PointsRewarder : Rewarder
{
    [SerializeField] private VecInt pointsRange;
    [SerializeField] private TextMesh textMesh;


    protected override IReward reward=> new PointsReward{points = points};

    public int points
    {
        get
        {
            
            return _points;
        }
        set
        {
            _points = value;
            textMesh.text = points.ToString();
        }
    }

    private int _points;

    private void Awake()
    {
        points = Random.Range(pointsRange.x, pointsRange.y);
    }

    public override IReward GetReward()
    {
        Destroy(gameObject);
        return reward;
    }

    public struct PointsReward:IPointsReward
    {
        public int points { get; set; }
    }
}