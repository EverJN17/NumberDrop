using System;
using UnityEngine;

namespace Game
{
    public class Utils
    {
        public static Color ValueToColor(int value, VecInt valueRange, Color[] colors)
        {
            var normalized = ((value - valueRange.x) + 0f) / (valueRange.y - valueRange.x);

            if (normalized < 0)
                throw new Exception("Normalized Cannot be negative");

            var baseIndex = (int)Mathf.Floor(normalized * (colors.Length - 1));
            if (baseIndex >= colors.Length - 1)
                return colors[colors.Length - 1];

            var balance = normalized - baseIndex * (1f / (colors.Length - 1));
            var lerb = balance / (1f / (colors.Length - 1));
            //        Debug.Log("Base Index:"+baseIndex);
            return Color.Lerp(colors[baseIndex], colors[baseIndex + 1], lerb);


        }
    }

}
