﻿using System.Collections;
using MyGame;
using UnityEngine;
using UnityEngine.UI;
using EasyMobile;
namespace Game
{
    public class GamePlayPanel : ShowHidable
    {
        [SerializeField] private Text _scoreTxt,_bestTxt;

        private LevelManager LevelManager => LevelManager.Instance;

       
        private void Start()
        {
           Advertising.ShowBannerAd(BannerAdPosition.Bottom);
           Debug.Log("Start GaemplayPanelShowBanner");
        }



        private void Update()
        {
            _scoreTxt.text = LevelManager.Score.ToString();
            _bestTxt.text = GameManager.BEST_SCORE.ToString();
        }

        public void OnClickRestart()
        {
            GameManager.LoadGame(new LoadGameData
            {
                GameType = GameType.Restart
            });

            bool isReady = Advertising.IsInterstitialAdReady();

// Show it if it's ready
            if (isReady)
            {
                Advertising.ShowInterstitialAd();
            }
        }

        public void OnClickPause()
        {
            
            Time.timeScale = 0f;
            UIManager.Instance.PausePanel.Show();
        }
        
    }
}