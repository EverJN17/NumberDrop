﻿using System;
using System.Collections;
using MyGame;
using UnityEngine;

namespace Game
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private GameOverPanel _gameOverPanel;

//        [SerializeField] private LevelCompletePanel _levelCompletePanel;
        [SerializeField] private MenuPanel _menuPanel;

        [SerializeField] private GamePlayPanel _gamePlayPanel;
        [SerializeField] private PausePanel _pausePanel;
        [SerializeField] private ContinuePanel _continuePanel;
            

        public GameOverPanel GameOverPanel => _gameOverPanel;
        public MenuPanel MenuPanel => _menuPanel;
        public GamePlayPanel GamePlayPanel => _gamePlayPanel;

        public PausePanel PausePanel => _pausePanel;
        public static UIManager Instance { get; private set; }

//        public PlatformSkinSelectionPanel PlatformSkinSelectionPanel => _platformSkinSelectionPanel;

        private void Awake()
        {
            Instance = this;

//            if(!AdsManager.HaveSetupConsent)
//                SharedUIManager.ConsentPanel.Show();

#if DAILY_REWARD
            if (MyGame.GameManager.HasPendingDailyReward)
            {
                var dailyRewardPanel = SharedUIManager.DailyRewardPanel;
                dailyRewardPanel.Show();
            }
#endif
        }



        private void OnDisable()
        {
            LevelManager.GameOver -= LevelManagerStageGroupResulted;
        }

        private void OnEnable()
        {
            LevelManager.GameOver += LevelManagerStageGroupResulted;
        }


        private void LevelManagerStageGroupResulted()
        {
            StartCoroutine(GameOver());
        }

        IEnumerator GameOver()
        {
            yield return new WaitForSeconds(0.1f);

            if (AdsManager.IsVideoAvailable())
                _continuePanel.Show();
            else
                _gameOverPanel.Show();
        }

    }
}