﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTilePart : MonoBehaviour,IPart
{
    private bool startedMoving;

    public void SetPosition(Vector2 position)
    {
        if (!startedMoving)
        {
            if (position.y<transform.position.y)
            {
                return;
            }
            startedMoving = true;
        }
        var transformPosition = transform.position;
        transformPosition.x = position.x;
        transformPosition.y = position.y;
        transform.position = transformPosition;
    }

    
}



public interface IPart:IUnityObject
{
    void SetPosition(Vector2 position);
}

public interface IUnityObject
{
    Transform transform { get; }
    GameObject gameObject { get; }
}