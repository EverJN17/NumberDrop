﻿public interface IPoolObjEventReciever
{
    void OnPoolObjInstantiate();
    void OnPoolObjDestroy();
}