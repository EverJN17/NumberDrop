﻿using UnityEngine;
using UnityEngine.UI;
using EasyMobile;
namespace Game
{
    public class MenuPanel : ShowHidable
    {

    	private void Start()
        {
           Advertising.ShowBannerAd(BannerAdPosition.Bottom);
           Debug.Log("MenuPanel.cs Show Banner");
        }

        public void OnClickPlay()
        {
            Hide();
            LevelManager.Instance.StartTheGame();
        }

    }
}