﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static event Action<Direction> DragDetected;

    [SerializeField]private Vector2 _minAndMaxTimeForDragDetect;
    [SerializeField] private float _minDelta;

    private Camera _camera;

    private Vector2 _lastPosition;

    private readonly List<Vector2> _deltaBuffer = new List<Vector2>();

    private bool _detectingDrag;
    private float _startDragTime;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _lastPosition = _camera.ScreenToWorldPoint(Input.mousePosition.WithZ(5));
            _detectingDrag = true;
            _startDragTime = Time.time;
            _deltaBuffer.Clear();
        }
        else if (Input.GetMouseButton(0) && _detectingDrag)
        {
            var point = _camera.ScreenToWorldPoint(Input.mousePosition.WithZ(5));
            _deltaBuffer.Add(((Vector2)point-_lastPosition) / Time.deltaTime);
            _lastPosition = point;

            if(Time.time - _startDragTime > _minAndMaxTimeForDragDetect.y)
                DetectDrag();
        }

        if (Input.GetMouseButtonUp(0) && _detectingDrag)
        {
            DetectDrag();
        }
    }

    private void DetectDrag()
    {
        if (!_detectingDrag)
        {
            return;
        }

        _detectingDrag = false;
        var delta = Time.time - _startDragTime;
        if(delta<_minAndMaxTimeForDragDetect.x || _deltaBuffer.Count == 0)
            return;

        var avgDelta = (_deltaBuffer.Aggregate(Vector2.zero,(sum, d) =>  sum + d)/_deltaBuffer.Count);
//        Debug.Log(_deltaBuffer.Aggregate(Vector2.one, (sum, d) => sum + d));
        _deltaBuffer.Clear();

        if (avgDelta.magnitude<_minDelta)
            return;



        var angle = Vector2.Angle(avgDelta,Vector2.right);

        if (angle<45 || angle>135f)
        {
            DragDetected?.Invoke(Vector2.Dot(avgDelta, Vector2.right) > 0 ? Direction.Right : Direction.Left);
        }
        else
        {
            DragDetected?.Invoke(Vector2.Dot(avgDelta, Vector2.up) > 0 ? Direction.Up : Direction.Down);
        }
    }

    public enum Direction
    {
        Up,Down,Left,Right
    }
}