using MyGame;
using UnityEngine;

public class PausePanel : ShowHidable
{
    public void OnClickContinue()
    {
        Time.timeScale = 1f;
        Hide();
    }

    public void OnClickRestart()
    {
        Time.timeScale = 1f;
        GameManager.LoadGame(new LoadGameData
        {
            GameType = GameType.Restart
        });
    }
}