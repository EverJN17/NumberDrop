﻿using UnityEngine;
[DefaultExecutionOrder(10000)]
public class CameraFollower : MonoBehaviour
{
    [SerializeField]private Transform target;
    [SerializeField] private Offset offset;

	void FixedUpdate ()
	{
	    var position = transform.position;
	    position.y = target.position.y+GetRealOffset(offset).y;
	    transform.position = position;
	}

    public enum OffSetType
    {
        WORLD,RELATIVE
    }
    

    private Vector3 GetRealOffset(Offset offset)
    {
        if (offset.offSetType == OffSetType.WORLD)
            return offset.offset;

        var height = Camera.main.orthographicSize;
        var width = Camera.main.orthographicSize * Screen.width / Screen.height;

        offset.offset.x = offset.offset.x * width;
        offset.offset.y = offset.offset.y * height;

        return offset.offset;
    }

    [System.Serializable]
    public struct Offset
    {
        public OffSetType offSetType;
        public Vector3 offset;
    }
}
