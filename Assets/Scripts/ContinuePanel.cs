

using System;
using Game;
using UnityEngine;
using UnityEngine.UI;

public class ContinuePanel : ShowHidable
{

    [SerializeField] private TimeOutProgressBar _continueBtn;
    [SerializeField] private Button _skipBtn;

    private void Awake()
    {
        _continueBtn.TimeOut += bar => OnClickSkip();
    }

    public override void Show(bool animate = true, Action completed = null)
    {
        base.Show(animate, completed);
        _skipBtn.gameObject.SetActive(false);
        Invoke(nameof(ShowSkip),_continueBtn.MaxTimeOut*0.5f);
    }

    private void ShowSkip()
    {
        _skipBtn.gameObject.SetActive(true);
    }

    public void OnClickContinue()
    {
        Debug.Log(nameof(OnClickContinue));
        AdsManager.ShowVideoAds(true, success =>
        {
            if(success)
            {
                LevelManager.Instance.ContinueTheGame();
                Hide();
            }
        });
    }

    public void OnClickSkip()
    {
        Hide();
        UIManager.Instance.GameOverPanel.Show();
    }
}