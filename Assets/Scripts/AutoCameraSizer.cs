﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class AutoCameraSizer : MonoBehaviour
{
    [SerializeField]private float _minWidth,_minHeight;
    private Camera mCamera;


    private void Awake()
    {
        mCamera = GetComponent<Camera>();

        var minAspect = _minHeight / _minWidth;
        var aspect = (float)Screen.height/Screen.width;

        if (aspect > minAspect)
        {
            mCamera.orthographicSize = 0.5f * _minWidth * aspect;
        }
        else
        {
            mCamera.orthographicSize = 0.5f * _minHeight;
        }
        
    }

}
