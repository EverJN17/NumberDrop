﻿using System;
using System.Runtime.CompilerServices;
using MyGame;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    public static event Action<int> OnScoreChanged;
    public static event Action GameOver; 
    public static event Action GameStarted;
    public static event Action GameContinued;

    #region SerizableFields

    [SerializeField] private Board _board;

    #endregion

    #region Properties

    public State CurrentState { get; private set; }
    public int Score
    {
        get => _score;
        private set
        {
            if (_score!=value)
            {
                _score = value;
                OnScoreChanged?.Invoke(_score);
            }
        }
    }


    public GameType GameType { get; private set; }
    public bool BestScoreArchived { get;private set; }

    #endregion

    private int _score;


    void Awake()
    {
        Instance = this;
        GameType = GameManager.LoadGameData.GameType;
        _board.Filled += BoardOnFilled;
        _board.MergedNumber+=BoardOnMergedNumber;
      
    }




    private void BoardOnMergedNumber(Block block)
    {
        Score += block.Number;

        if (GameManager.BEST_SCORE < Score)
            GameManager.BEST_SCORE = Score;
    }

    private void BoardOnFilled()
    {
        OverTheGame();
    }
    


   

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartTheGame();
        }


    }


    public void StartTheGame()
    {
        if(CurrentState!=State.WaitingForPlay)
            throw new Exception("The current state should be Waiting for play to start the game");
        
        CurrentState = State.Playing;
        StartCoroutine(SimpleCoroutine.WaitFrame(() => _board.StartDroppingBlocks()));
        GameStarted?.Invoke();
    }



    public void ContinueTheGame()
    {
        CurrentState = State.Playing;
        StartCoroutine(_board.ContinueTheGame());
        GameContinued?.Invoke();
    }



    void OverTheGame()
    {
        if(GameManager.BEST_SCORE<Score)
        {
            GameManager.BEST_SCORE = (Score);
            BestScoreArchived = true;
        }

        CurrentState = State.GameOver;
        GameOver?.Invoke();
    }
    

    public enum State
    {
        WaitingForPlay,Playing,GameOver
    }
}
