﻿// /*
// Created by Darsan
// */

using UnityEngine;

public class SharedUIManager : Singleton<SharedUIManager>
{
    [SerializeField] private LoadingPanel _loadingPanel;
    [SerializeField] private RatingPopUp _ratingPopUp;
//    [SerializeField] private DailyRewardPanel _dailyRewardPanel;
    [SerializeField] private PopUpPanel _popUpPanel;
    [SerializeField] private ConsentPanel _consentPanel;
//    [SerializeField] private DailyRewardPanel _dailyRewardPanel;


//    public static DailyRewardPanel DailyRewardPanel => Instance._dailyRewardPanel;
    public static ConsentPanel ConsentPanel => Instance._consentPanel;
    public static PopUpPanel PopUpPanel => Instance._popUpPanel;
//    public static DailyRewardPanel DailyRewardPanel => Instance._dailyRewardPanel;
    public static LoadingPanel LoadingPanel => Instance?._loadingPanel;
    public static RatingPopUp RatingPopUp => Instance?._ratingPopUp;
}