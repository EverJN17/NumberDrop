﻿using UnityEngine;

public class FpsViewer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        GUI.color = Color.white;
        GUI.Label(new Rect(10,10,150,30),"Fps:"+(1/Time.deltaTime));
    }
}