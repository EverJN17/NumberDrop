﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Application = UnityEngine.Application;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class Board : MonoBehaviour
{
    public event Action Filled;
    public event Action<Block> MergedNumber;

    [SerializeField] private Vector2Int _gridSize;
    [SerializeField] private float _boardWidth;
    [SerializeField] private float _spacing;
    [SerializeField] private float _padding;
    [SerializeField] private float _bottomOffset;



    [SerializeField] private Block _blockPrefab;
    [SerializeField] private Transform _boardTop;
    [SerializeField] private float _pendingBlockDropSpeed;
    [SerializeField] private float _confirmBlockDropSpeed;
    [SerializeField] private Block _nextBlock;
    [SerializeField] private AudioClip _mergeClip, _dropClip;
    [SerializeField] private SpriteRenderer _highlightEffect;
    [SerializeField] private int _continueDestroyRowCount;
    
    
    private int _lastDropIndex = -1;
    private readonly List<GridHolder> _grids = new List<GridHolder>();
    
    
    public GridHolder this[int x, int y] => _grids[y * _gridSize.x + x];
    public GridHolder this[Vector2Int pos] => this[pos.x, pos.y];
    public float TileSize { get; private set; }

    public Block DroppingBlock { get; private set; }
    public bool Processing { get; private set; }

    public int NextBlock { get; private set; }
    public DropBlockState CurrentDropBlockState { get; private set; }
    public Block LastMergedBlock { get; private set; }
    public bool IsBoardFilled { get; private set; }

    public bool HasStartedDropping { get; private set; }


    private void Awake()
    {
        TileSize = (_boardWidth - (_gridSize.x - 1) * _spacing - _padding * 2) / _gridSize.x;
        GenerateGrid();
        SetRandomNextBlockValue();
    }


    private void GenerateGrid()
    {
        var bottomLeft = -Vector3.up * _bottomOffset + Vector3.up * _padding -
            Vector3.right * _boardWidth / 2 + Vector3.right * _padding + new Vector3(TileSize, TileSize) / 2f;

        Debug.Log("bottom left:"+bottomLeft);
        for (var i = 0; i < _gridSize.y; i++)
        {
            for (var j = 0; j < _gridSize.x; j++)
            {
                var holder = new GridHolder
                {
                    Coordinate = new Vector2Int(j, i),
                    Board = transform,
                    LocalPosition = bottomLeft +
                                    j * (_spacing + TileSize) * Vector3.right +
                                    i * (_spacing + TileSize) * Vector3.up
                };
                _grids.Add(holder);
            }
        }
    }

    public void StartDroppingBlocks()
    {
        if (HasStartedDropping)
            return;
        HasStartedDropping = true;
        DropNextNumber();
    }


    private void DropNextNumber()
    {
       
        if (_lastDropIndex < 0)
            _lastDropIndex = Random.Range(0, _gridSize.x);

        DroppingBlock = Instantiate(_blockPrefab,
            _boardTop.transform.position.WithX(this[_lastDropIndex, 0].WorldPosition.x), Quaternion.identity);
        DroppingBlock.Number = NextBlock;
        DroppingBlock.Size = TileSize;
        CurrentDropBlockState = DropBlockState.Pending;
        SetRandomNextBlockValue();
    }

    private void SetRandomNextBlockValue()
    {
        NextBlock = GetNextBlockValue();
        _nextBlock.Number = NextBlock;
    }

    private void Update()
    {
        HandleInput();
        HandleUpdateDroppingBlock();
    }

    private void HandleInput()
    {
        if(DroppingBlock==null || CurrentDropBlockState != DropBlockState.Pending )
            return;
        
        if(
            #if UNITY_EDITOR
            EventSystem.current.IsPointerOverGameObject()
            #elif UNITY_ANDROID || UNITY_IOS
            Input.touchCount >0 && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)
            #endif
        )
            return;

        
        if (Input.GetMouseButtonDown(0)||Input.GetMouseButton(0))
        {
            var worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var x = GetNearestXForPosition(worldPoint);
            if (x < _gridSize.x && x>=0)
            {
                if(CanMoveDroppingBlockToIndex(x))
                MoveDroppingBlockToIndex(x);
            }
        }

        if (Input.GetMouseButtonUp(0))
        { 
            var worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var x = GetNearestXForPosition(worldPoint);
            
            if(x>=0 && x<_gridSize.x && Mathf.Abs(this[x,0].WorldPosition.x - DroppingBlock.transform.position.x)<float.Epsilon)
            {
                ConfirmDroppingBlock();
            }
        }
    }

    private void ConfirmDroppingBlock()
    {
        CurrentDropBlockState = DropBlockState.Confirm;
        _highlightEffect.color = DroppingBlock.Renderer.color.WithAlpha(0.1f);
        _highlightEffect.transform.position =
            _highlightEffect.transform.position.WithX(this[_lastDropIndex, 0].WorldPosition.x);
        _highlightEffect.gameObject.SetActive(true);
    }


    private int GetNearestXForPosition(Vector2 position)
    {
        return Mathf.RoundToInt((position.x - (-_boardWidth / 2+transform.position.x + _padding + TileSize / 2)) / (TileSize + _spacing));
    }
    private void HandleUpdateDroppingBlock()
    {
        if (DroppingBlock == null) return;


        if (CurrentDropBlockState != DropBlockState.None)
        {
            var speed = CurrentDropBlockState == DropBlockState.Pending
                ? _pendingBlockDropSpeed
                : _confirmBlockDropSpeed;

            var position = DroppingBlock.transform.position;
            position.y -= speed * Time.deltaTime;

            // ReSharper disable once Unity.InefficientPropertyAccess
            DroppingBlock.transform.position = position;
        }

        var nearestHolder = GetNearestHolder(DroppingBlock.transform.position, out var distance);
        //Debug.Log("nearestHolder:" + nearestHolder.Coordinate);

        if ((nearestHolder.WorldPosition.y < DroppingBlock.transform.position.y && distance < 0.01f) ||
            (nearestHolder.WorldPosition.y > DroppingBlock.transform.position.y))
        {
            while (nearestHolder.Block!=null && nearestHolder.Coordinate.y < _gridSize.y-1 )
            {
                nearestHolder = this[nearestHolder.Coordinate + Vector2Int.up];
            }

            if (nearestHolder.Coordinate.y == 0 || this[nearestHolder.Coordinate - Vector2Int.up].Block != null)
            {
                OnDroppingBlockDropped(nearestHolder);
            }
        }
    }

    private void OnDroppingBlockDropped(GridHolder nearestHolder)
    {
        DroppingBlock.transform.position = nearestHolder.WorldPosition;
        SetBlockToHolder(DroppingBlock, nearestHolder);
        _highlightEffect.gameObject.SetActive(false);
        OnBlockDropped(DroppingBlock);
        StartCoroutine(HandleBlockMerge());
    }

    private void OnBlockDropped(Block droppingBlock)
    {
        StartCoroutine(droppingBlock.DropAnim());
        PlayClipIfCan(_dropClip);
    }

    private void PlayClipIfCan(AudioClip clip, float volume = 1f)
    {
        if(!AudioManager.IsSoundEnable|| clip ==null)
            return;
        AudioSource.PlayClipAtPoint(clip,Camera.main.transform.position,volume);
    }

    private void SetBlockToHolder(Block block, GridHolder holder)
    {
        if (holder != null)
            holder.Block = block;

        if (block != null)
            block.Holder = holder;
    }

    private IEnumerator HandleBlockMerge()
    {
        //Debug.Log(nameof(HandleBlockMerge));
        var blockMergeDatas = GetCurrentMerges().ToList();
        DroppingBlock = null;
        CurrentDropBlockState = DropBlockState.None;
        Processing = true;
        do
        {
            if (blockMergeDatas.Any())
            {
                yield return SimpleCoroutine.MergeParallel(blockMergeDatas.Select(d => MergeBlock(d)), this);
                LastMergedBlock = blockMergeDatas.Last().FirstBlock;
            }

            yield return DropHangingBlocks();
            yield return null;
            blockMergeDatas = GetCurrentMerges().ToList();
        } while (blockMergeDatas.Any());

        LastMergedBlock = null;
        Processing = false;


        if (HasBoardFilled())
        {
            MarkAsBoardFilled();
            yield break;
        }
        else
        {
            DropNextNumber();
        }
    }

    private void MarkAsBoardFilled()
    {
        IsBoardFilled = true;
        Filled?.Invoke();
    }


    private bool HasBoardFilled()
    {
        return Enumerable.Range(0, _gridSize.x)
            .Any(x => Enumerable.Range(0, _gridSize.y).All(y => this[x, y].Block != null));
    }


    private IEnumerable<BlockMergeData> GetCurrentMerges()
    {
        if (LastMergedBlock == null)
            LastMergedBlock = DroppingBlock;

        if (LastMergedBlock == null)
            throw new InvalidDataException();

        var pendingBlocksFroMerge = _grids.Where(g => g.Block != null).Select(g => g.Block)
            .Except(new[] {LastMergedBlock}).ToList();
        pendingBlocksFroMerge.Insert(0,LastMergedBlock);

        while (pendingBlocksFroMerge.Count > 0)
        {
            var holder = pendingBlocksFroMerge.First().Holder;
            foreach (var gridHolder in GetAdjacentHolderInOrder(holder.Coordinate))
            {
                if (gridHolder.Block != null && pendingBlocksFroMerge.Contains(gridHolder.Block) &&
                    gridHolder.Block.Number == holder.Block.Number)
                {
                    var firstHolder = gridHolder.Coordinate.y > holder.Coordinate.y ? gridHolder : holder;
                    var secondHolder = gridHolder == firstHolder ? holder : gridHolder;

                    yield return new BlockMergeData
                    {
                        FirstBlock = firstHolder.Block,
                        SecondBlock = secondHolder.Block
                    };

                    pendingBlocksFroMerge.Remove(gridHolder.Block);
                    break;
                }
            }

            pendingBlocksFroMerge.Remove(holder.Block);
        }
    }

    private IEnumerable<GridHolder> GetAdjacentHolderInOrder(Vector2Int coordinate)
    {
        if (coordinate.y > 0)
            yield return this[coordinate - Vector2Int.up];

        if (coordinate.y < _gridSize.y - 1)
            yield return this[coordinate + Vector2Int.up];

        if (coordinate.x < _gridSize.x - 1)
            yield return this[coordinate + Vector2Int.right];

        if (coordinate.x > 0)
            yield return this[coordinate - Vector2Int.right];
    }

    private IEnumerator DropHangingBlocks()
    {
        var list = new List<IEnumerator>();
        for (var i = 0; i < _gridSize.x; i++)
        {
            var emptyIndex = -1;
            for (var j = 0; j < _gridSize.y; j++)
            {
                if (emptyIndex < 0 && this[i, j].Block == null)
                {
                    emptyIndex = j;
                }
                else if (emptyIndex >= 0 && this[i, j].Block != null)
                {
                    list.Add(DropBlockTo(this[i, j].Block, this[i, emptyIndex]));
                    emptyIndex++;
                }
            }
        }

        if (list.Any())
            yield return SimpleCoroutine.MergeParallel(list, this);
    }


    private IEnumerator DropBlockTo(Block block, GridHolder holder, float speed = 5f)
    {
        var startPoint = block.transform.position;
        var targetPoint = holder.WorldPosition;
        yield return SimpleCoroutine.MoveTowardsEnumerator(
            onCallOnFrame: n => { block.transform.position = Vector3.Lerp(startPoint, targetPoint, n); }, speed: speed);

        if (block.Holder.Block == block)
            SetBlockToHolder(null, block.Holder);

        SetBlockToHolder(block, holder);
        OnBlockDropped(block);
    }

    private IEnumerator MergeBlock(BlockMergeData data, float speed = 5f)
    {
        var startPoint = data.SecondBlock.transform.position;
        var targetPoint = (data.FirstBlock.transform.position - startPoint) * 0.8f + startPoint;
        var haveDoubled = false;
        yield return SimpleCoroutine.MoveTowardsEnumerator(onCallOnFrame: n =>
        {
            if (n > 0.5f && !haveDoubled)
            {
                StartCoroutine(data.FirstBlock.DoubleTheNumberWithAnim(2 * speed));
                haveDoubled = true;
            }

            data.SecondBlock.transform.position = Vector3.Lerp(startPoint, targetPoint, n);
        }, speed: speed);

        SetBlockToHolder(null, data.SecondBlock.Holder);
        PlayClipIfCan(_mergeClip);
        Destroy(data.SecondBlock.gameObject);
        MergedNumber?.Invoke(data.FirstBlock);
    }

    private GridHolder GetNearestHolder(Vector2 position, out float distance)
    {
        //TODO:Improve later for performance
        var gridHolder = _grids.OrderBy(holder => (holder.WorldPosition - position).sqrMagnitude).First();
        distance = (position - gridHolder.WorldPosition).magnitude;
        return gridHolder;
    }


    private int GetNextBlockValue()
    {
       
        var maxValue = _grids.Any(g => g.Block != null)?_grids.Where(g => g.Block != null).Max(g => g.Block.Number):2;
        return Mathf.RoundToInt(Mathf.Pow(2,Random.Range(1,(int)Mathf.Log(maxValue,2)+1)));
    }


    private bool CanMoveDroppingBlockToIndex(int index)
    {
        var topBlock = Enumerable.Range(0,_gridSize.y).Select(i=>this[index,i]).Where(g=>g.Block!=null).OrderBy(g=>g.Coordinate.y).LastOrDefault();
        return topBlock == null || topBlock.WorldPosition.y + TileSize < DroppingBlock.transform.position.y;
    }

    private void MoveDroppingBlockToIndex(int index)
    {
        // ReSharper disable once Unity.InefficientPropertyAccess
        DroppingBlock.transform.position = DroppingBlock.transform.position.WithX(this[index, 0].WorldPosition.x);
        _lastDropIndex = index;
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.TransformPoint(-Vector3.up * _bottomOffset), 0.1f);
        Gizmos.DrawWireCube(transform.position, new Vector3(_boardWidth, _boardWidth));

        if (Application.isPlaying)
        {
            Gizmos.color = Color.green;
            _grids.ForEach(g => Gizmos.DrawWireSphere(g.WorldPosition, 0.1f));
        }
    }

    public IEnumerator ContinueTheGame()
    {
        yield return SimpleCoroutine.MergeParallel(Enumerable.Range(0, _continueDestroyRowCount).Select(DestroyRow),
            this);

        yield return DropHangingBlocks();
        DropNextNumber();
    }

    private IEnumerator DestroyRow(int index)
    {
        for (int i = 0; i < _gridSize.x; i++)
        {
            var gridHolder = this[i,index];
            if(gridHolder.Block!=null)
            {
                Destroy(gridHolder.Block.gameObject);
                SetBlockToHolder(null,gridHolder);
                yield return new WaitForSeconds(0.05f);
            }
        }
    }
    
    public enum DropBlockState
    {
        None,
        Pending,
        Confirm
    }

    public struct BlockMergeData
    {
        public Block FirstBlock { get; set; }
        public Block SecondBlock { get; set; }
    }


}

public class GridHolder
{
    public Vector2Int Coordinate { get; set; }
    public Block Block { get; set; }
    public Vector2 LocalPosition { get; set; }
    public Transform Board { get; set; }
    public Vector2 WorldPosition => Board.TransformPoint(LocalPosition);
}