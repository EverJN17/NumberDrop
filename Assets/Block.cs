﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] private TextMesh _text;
    [SerializeField] private SpriteRenderer _renderer;
    [SerializeField] private List<Color> _colors = new List<Color>();
    [SerializeField] private SpriteRenderer _highlightRenderer;
    [SerializeField] private ParticleSystem _dropParticles;
    [SerializeField] private GameObject _doubleHighlighter;
    [SerializeField]
    private List<TextScaleAndLetterCount> _textScaleAndLetterCounts = new List<TextScaleAndLetterCount>();
    
    
    private int _number;
    public IEnumerable<Color> Colors => _colors;

    public SpriteRenderer Renderer => _renderer;
    public GridHolder Holder
    {
        get;
        set;
    }

    public int Number
    {
        get => _number;
        set
        {
            _number = value;
            _text.text = value.ToString();
            var power = (int)Mathf.Log(value,2);
            _renderer.color = Colors.ElementAt((power-1) % Colors.Count());
            UpdateTextScale();
        }
    }

    public float Size { get; set; }

    private void UpdateTextScale()
    {
        var index = _textScaleAndLetterCounts.Count;

        for (var i = 0; i < _textScaleAndLetterCounts.Count; i++)
        {
            if (_textScaleAndLetterCounts[i].letterCount > _text.text.Length)
            {
                index = i;
                break;
            }
        }

        _text.transform.localScale = Vector3.one * _textScaleAndLetterCounts[index - 1].textScale;
    }

    public IEnumerator DoubleTheNumberWithAnim(float speed =5f)
    {
        var startScale = transform.localScale.x;
        var startColor = _renderer.color;
        var targetScale = startScale * 1.1f;
        yield return SimpleCoroutine.MoveTowardsEnumerator(onCallOnFrame: n =>
        {
            transform.localScale = Vector3.one * Mathf.Lerp(startScale, targetScale, n);
            _renderer.color = Color.Lerp(startColor, Color.white,n);
        },speed:speed*2);

        Number *= 2;
        startColor = _renderer.color;
        yield return SimpleCoroutine.MoveTowardsEnumerator(onCallOnFrame: n =>
        {
            transform.localScale = Vector3.one * Mathf.Lerp(targetScale, startScale, n);
            _renderer.color = Color.Lerp( Color.white,startColor,n);
        },speed:speed*2);
        _doubleHighlighter.SetActive(true);

        yield return new WaitForSeconds(0.5f);
        _doubleHighlighter.SetActive(false);
    }

    public IEnumerator DropAnim()
    {
        _highlightRenderer.gameObject.SetActive(true);
        if (_dropParticles)
            Instantiate(_dropParticles, transform.position.WithY(transform.position.y - 0.9f*Size / 2f),
                Quaternion.identity);
        yield return SimpleCoroutine.MoveTowardsEnumerator(onCallOnFrame: n =>
        {
            _highlightRenderer.color = _highlightRenderer.color.WithAlpha(Mathf.Lerp(0, 1f, n));
        }, speed: 10);
        yield return new WaitForSeconds(0.05f);
        yield return SimpleCoroutine.MoveTowardsEnumerator(onCallOnFrame: n =>
        {
            _highlightRenderer.color = _highlightRenderer.color.WithAlpha(Mathf.Lerp(1f, 0f, n));
        }, speed: 10f);
        
        _highlightRenderer.gameObject.SetActive(false);

    }
    

    [Serializable]
    public struct TextScaleAndLetterCount
    {
        public int letterCount;
        public float textScale;
    }
}


